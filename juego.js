class Ahorcado{

    constructor(){
        this.palabras = ["argumento","parametros","console","uml","comportamiento","label","inner","input", "html", "clases", "repositorio", "commit", "canvas", "paradigma", "ciclos", "funciones", "constructor", "patron", "cohesion", "interfaz", "dom", "decisiones", "string", "boolean", "round",  ];
        this.ayudas = ["Parámetros", "Variable utilizada para recibir valores de entrada", "Es una Web Api", "Lenguaje de modelado más conocido", "Definido por métodos" ,"Etiqueta para un elemento en una interfaz de usuario", "Devuelve o establece la sintaxis HTML" ,"Se usa para crear controles interactivos para formularios basados en la web, que reciban datos del usuario", "Lenguaje de Marcas de Hipertexto", "Descripción para objetos", "Control de versiones", " Confirmar un conjunto de cambios provisionales de forma permanente", "Usado para dibujar gráficos usando scripts", "Forma aceptada de estructurar la programación", "Secuencia que se ejecuta repetidas veces", "Hacen una tarea en concreto", "Crear o inicializar una clase", " Técnicas para resolver problemas", "Grado en que los elementos de un módulo permanecen juntos", "Comunicación con el usuario", "Interfaz de programación para los documentos HTML", "Selecciona la siguiente instrucción a ejecutar", "Cadena de caracteres", "Falso o Verdadero", "...Al entero más cercano",  ]
        this.poPalabra;
        this.palabra;
        this.ayuda;
        this.abc;
        this.casos;
        this.cont = 6;
        this.generarPalabra();
        this.dibujarTablero();
        this.dibujarLetras();
        this.dividirPalabra();
    }

    generarPalabra(){
        let n;
        n=(Math.random() * (24 - 0)) + 0;
        n=Math.round(n);
        this.poPalabra = n;
        return this.pPalabra;   
    }

    dividirPalabra(){
        this.palabra = this.palabras[this.poPalabra];
        this.palabra = this.palabra.split("");
        document.getElementById('nletras').innerHTML=this.palabra.length; 
        console.log(this.palabra);
        return this.palabra; 

    }

    dibujarTablero() {

        let miTablero = document.getElementById("tablero");

        for (let i = 0; i < this.palabras[this.poPalabra].length; i++) {
            miTablero.innerHTML = miTablero.innerHTML + "<input type='text' id='casilla"+(i)+"' class='espacio' value='' style='color: transparent;text-shadow: 0 0 0 black;'>";
        }
        console.log(this.palabras[this.poPalabra]);
    }

    ayudar(){
        this.ayuda = this.ayudas[this.poPalabra];
        document.getElementById('pista').innerHTML=this.ayuda; 

    }

    dibujarLetras(){
    
        let abecedario = document.getElementById("abecedario");
        let i = "a".charCodeAt(0);
        let j = "z".charCodeAt(0);
        let Ñ = String.fromCharCode("ñ".charCodeAt(0));
        for ( ; i<= j; i++){
            this.abc = String.fromCharCode(i);
            abecedario.innerHTML = abecedario.innerHTML +"<input type='button' id='letra"+this.abc+"' value='"+this.abc+"' onclick='jugar.intento()' class='letra' style='color: transparent;text-shadow: 0 0 0 black;' ></input>"
        }
        abecedario.innerHTML = abecedario.innerHTML +"<input type='button' id='letra"+Ñ+"' value='"+Ñ+"' onclick='jugar.intento()' class='letra' style='color: transparent;text-shadow: 0 0 0 black;' ></input>"
    }

    intento(){
        
        let miElemento=event.target;
        if (this.palabra.indexOf(miElemento.value)!= -1) {
            for (let i = 0; i<= this.palabra.length; i++){
                let n = document.getElementById("casilla"+i)
                if (this.palabra[i]===miElemento.value){
                    n.value = miElemento.value;
                }
            }
        } else {
            this.cont --;
            document.getElementById('intentos').innerHTML="Quedan "+this.cont+" intentos"; 

        }   
    }



}   